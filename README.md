# README

## What is this app?

This is an app made it with Angular and Symfony, it's about a really simple task manager with a kanban board.

This is my masters final project about full stack developing. I decided to upload it because it may be useful to someone
that is beginning in this world. 

## What technologies are used to get works this app?

### Infrastructure

This project is based on docker and docker-compose servers, if you don't know docker and docker-compose, an advice,
just read this [link](https://www.docker.com/) and this [link](https://docs.docker.com/compose/) to know them, 
you are losing a wonderful tool.

To build this app need a couple of servers, with docker and docker-compose is really easy to achieve this, 
the servers are:

- BackEnd linux:
    - Apache -> Web server
    - PHP -> Web server
    - MySql/MariaDb -> Data base server
- FrontEnd linux:
    - NodeJs -> Javascript web server 
- Repository:
    - GitHub

### Frameworks

- BackEnd:
    - Symfony 4
    - Doctrine
    - Composer
- FrontEnd:
    - Angular 8
    - Bootstrap 4
    
## Can I run it in my local machine and how?

Short answer is yes, but bear in mind that you need follow the next steps:

I have a linux machine but for a windows machine is similar, I don't explain to you how install docker and docker compose,
I just tell you about how configure and start this project.

#### Local web server

You need to add to the hosts file simpletaskfront.localhost.test and simpletaskfront.localhost.test (for example)
hosts pointing to localhost, you need also a local web server, I've a nginx web server, with the follow configuration:  

##### Front

```
server {
  listen       80;
  server_name  simpletaskfront.localhost.test;
  location / {
    proxy_pass http://127.0.0.1:4200;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
}
```

##### Back

```
server {
  listen       80;
  server_name  simpletask.localhost.test;

  location / {
    proxy_pass http://127.0.0.1:8080;
  }
}

```

#### Database and mail configuration

Go to st-back folder and run a ```composer install``` command (you need to install it before, [get composer](https://getcomposer.org/)).

Then execute migrations with doctrine: ```bin/console d:m:m -n``` and load initial data with ```bin/console d:f:l -n``` 

Configure st-back/.env MAILER_URL variable with your mail info.

#### Front end

Go to st-front folder and run ```npm install``` command (so you need to install nodejs and npm before, [get node](https://nodejs.org/es/download/package-manager/))

## Can I check this app online?

You can check this app in [SimpleTasks](https://simple-tasks.samsoft.es), I uploaded to the cloud, but keep in mind that this is 
an example project, and it isn't a real world app, because of that ***all data will be erased every 2 hours (database data, uploaded files, etc).***

## There is some documentation?

Yes, you can find it in my [Nextcloud](https://nextcloud.samsoft.es/s/STV100_public), you can find in there a functional document,
a requirements document, an infrastructure document, and a user manual document, but all of these documents are in Spanish.

You can find the apps mockups in my [Presentator](https://presentator.samsoft.es/#/oMy4F8vO).

## The last but not least, how works the app?

The first registered user will be an admin user, an admin user can activate users, change some data and remove users,
at the moment this is the difference with a 'normal' (Logged) user basically.

All users can add columns, tasks, tags and edit their profiles.

Only for the backend through API calls, you can CRUD the sidebar menu options and projects. In the next version this tasks
will be in the front only for administrators.

That's all mates, regards!!!
