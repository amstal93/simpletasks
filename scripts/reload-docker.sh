#!/bin/bash
echo '///////////////////////////////////'
echo '////////     ////////     /////////'
echo '//////  /////  // // /////  ///////'
echo '////////     ///////     //////////'
echo '///////////////////////////////////'
echo ''
echo ''
echo '********Reload docker services*********'
echo ''
echo ''
cd ~/simpletasks-full
docker-compose -f docker-compose.prod.yml stop
echo ''
echo ''
echo 'updating git'
git fetch
git reset --hard
git checkout origin/master
git status
echo ''
echo ''
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
echo ''
echo ''
echo '********Finish reload docker services*********'
