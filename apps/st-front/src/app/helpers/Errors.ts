export class Errors {
  public static GENERAL_ERROR: string = "Some error happened retrieving data from server";
  public static UPLOAD_FILE_ERROR: string = "Some error uploading the file";
  public static NOT_LOGGED_ERROR = 'Not logged';
  public static TASKS_IN_COLUMN_ERROR = 'There are some tasks in the column, please change this tasks and then remove it';
  public static GET_SIDEBAR_OPTIONS_ERROR = 'Error retrieving side menu options';
  public static COLUMN_LIMIT_ERROR = 'Column limit reached';
  public static SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR = 'Service class type not defined';
  public static HASH_NOT_DEFINED_ERROR = 'Operation not allowed, hash undefined';
  public static PASSWORDS_MISMATCH_ERROR = 'Passwords must be equals';
  public static USER_ASSIGNMENT_ERROR = 'User assignment not valid, pick one from the suggestions list please';
  public static COLUMN_NOT_SET_ERROR = 'Please choose one';
  public static NO_COLUMNS_ERROR = 'To create tasks first you need to create a column';
  public static AVATAR_FORMAT_ERROR = 'Only jpeg, jpg or png are allowed';
  public static USER_WITH_TASKS_ERROR = 'Only unassigned users can be removed, please remove it from tasks and try again';
}
