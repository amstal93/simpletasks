import {HttpHeaders} from '@angular/common/http';

export class Cons {

  // BACKEND ROUTES /////////////////////////////////////////////////////////
  static showPath: string = '/show/';
  static findByNamePath: string = '/findbyname/';
  static findByTitlePath: string = '/findbytitle/';
  static listPath: string = '/list';
  static listByColumnPath: string = '/list-column/';
  static editPath: string = '/edit/';
  static removePath: string = '/remove/';
  static createPath: string = '/create';
  static uploadPathUnsecured: string = '/upload';
  static uploadPathSecured: string = '/supload';
  static forgotPassPath: string = '/reset-password';
  static forgotPassPathFinish: string = '/reset-password-finish/';

  // SPECIFIC BACKEND USER PATHS
  static registerPath: string = '/register';
  static loginPath: string = '/login';

  // FRONTEND ROUTES /////////////////////////////////////////////////////////
  static frontRegisterPath: string = '/register';
  static frontForgotPassPath: string = '/forgot-password';
  static frontForgotPassPathFinish: string = '/reset-password-finish/';
  static frontNewTaskPath: string = '/new-task';
  static frontUpdateTaskPath: string = '/update-task/';
  static frontTaskListPath: string = '/task-list';
  static frontProfileListPath: string = '/profile';
  static frontAdminUser: string = '/users-admin';

  // HTTP ///////////////////////////////////////////////////////////////////
  static unprivilegedHeader: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  });

  static privilegedHeader(auth: string): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Auth': auth
    });
  }
}
