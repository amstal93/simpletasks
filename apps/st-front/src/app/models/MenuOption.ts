export class MenuOption {
  constructor(
    public uuid: string,
    public title: string,
    public icon: string
  ) {}
}
