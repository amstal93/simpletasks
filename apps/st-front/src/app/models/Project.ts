export class Project {

  public uuid: string;
  public title: string;
  public description: string;
  public color: string;

  constructor() {}
}
