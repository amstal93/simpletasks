import {Role} from './Role';

export class User {

  public uuid: string;
  public name: string;
  public mail: string;
  public avatar?: string;
  public status?: number;
  public password: string;
  public role: Role;

  constructor() {}
}
