import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filterColumnTasks',
  pure: false
})
export class FilterColumnTasksPipe implements PipeTransform {

  transform(tasks: any[], filterColumn: any): any {

    // avoid errors
    if (!filterColumn || !tasks) {
      return null;
    }

    // only return col tasks
    return tasks.filter(task => {
      if (!task.column) return false;
      return task.column.uuid == filterColumn.uuid;
    });
  }

}
