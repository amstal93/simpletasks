import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-simple-modal',
  templateUrl: './simple-modal.component.html',
  styleUrls: []
})
export class SimpleModalComponent implements OnInit {

  // modal properties
  @Input() modal_title: string;
  @Input() modal_text: string;
  @Input() modal_button_text: string;

  // modal type flags
  @Input() is_error: boolean;
  @Input() is_warning: boolean;
  @Input() is_cancellable: boolean;

  // output close modal event
  @Output() closeModalEventEmitter: EventEmitter<boolean>;

  constructor() {
    // init data
    this.closeModalEventEmitter = new EventEmitter<boolean>();
  }

  /**
   * @inheritDoc
   */
  ngOnInit() { }

  /**
   * Close the modal and send an event
   */
  public closeModal() {
    // @ts-ignore
    $('#simpleModalDialog').modal('hide');
    this.closeModalEventEmitter.emit(false);
  }

  /**
   * Close the modal and send an event
   */
  public cancelModal() {
    // @ts-ignore
    $('#simpleModalDialog').modal('hide');
    this.closeModalEventEmitter.emit(true);
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Open the modal
   */
  public openModal() {
    // @ts-ignore
    $('#simpleModalDialog').modal({
        backdrop: 'static',
        keyboard: false
      });
  }
}
