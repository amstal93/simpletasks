import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Column} from '../../models/Column';

@Component({
  selector: 'app-add-column-modal',
  templateUrl: './add-column-modal.component.html',
  styleUrls: ['./add-column-modal.component.scss']
})
export class AddColumnModalComponent implements OnInit {
  // form
  form: FormGroup;
  submitted: boolean;

  // flag to know if it's a new column or update column
  is_update: boolean;

  // column to update
  @Input('column') column: Column;

  // modal event emitters
  @Output() closeModalEventEmitter: EventEmitter<Column> = new EventEmitter<Column>();
  @Output() cancelModalEventEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder) {
    // init data
    this.is_update = false;
    this.submitted = false;

    if (!this.column) {
      this.column = new Column();
      this.column.title = '';
    }
  }

  ngOnInit() {
    // form validators
    this.form = this.formBuilder.group({
      col_title: ['', [
        Validators.required,
        Validators.maxLength(75)
      ]],
    });
  }

  closeModal() {

    // change form status flag
    this.submitted = true;

    // if is invalid exit
    if (this.form.invalid) {
      return;
    }

    // else take data from the form
    this.column.title =
      this.f().col_title.value.trim();

    // @ts-ignore
    // close the dialog
    $('#columnModalDialog').modal('hide');

    // emit column with title data
    this.closeModalEventEmitter.emit(this.column);
  }

  /**
   * Close the dialog and emit event
   */
  cancelModal() {
    // @ts-ignore
    // close the modal
    $('#columnModalDialog').modal('hide');

    // emit cancel event
    this.cancelModalEventEmitter.emit();
  }


  // noinspection JSUnusedGlobalSymbols
  /**
   * Shows the modal
   */
  openModal(){
    // @ts-ignore
    $('#columnModalDialog').modal('show');
  }

  /**
   * Aux method to sort form controls calls
   */
  protected f(): any {
    return this.form.controls;
  }
}
