import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {UserService} from '../../services/user.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss'],
  providers: [UserService],
  animations: [
    trigger('fadeIn', [
      state('initial', style({
        opacity: 0
      })),
      state('final', style({
        opacity: 1
      })),
      transition('initial=>final', animate('1000ms'))
    ]),
  ]
})
export class MainContentComponent implements OnInit, OnChanges {

  // sidebar menu opened flag
  @Input() menuOpened: boolean;
  menuShowed: boolean;

  // animation
  animationState: string;

  constructor() {
    // init data
    this.menuShowed = false;
    this.animationState = 'initial';
  }

  /**
   * @inheritDoc
   */
  ngOnChanges(changes: {[propKey: string]: SimpleChange}): void {
    // observe menu flag to change main content element classes
    this.menuShowed =
      changes.menuOpened.firstChange ? false : changes.menuOpened.currentValue;
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {}

  /**
   * When user navigate between different paths
   * a fade animation is applied
   *
   * @param start boolean
   */
  changeAnimationState(start: boolean) {

    // if it's start event then program
    // the final animation state change
    if (start){
      setTimeout(() => {
        this.animationState = 'final';
      },100);
    }

    // set to initial animation state
    this.animationState = 'initial';

  }
}
