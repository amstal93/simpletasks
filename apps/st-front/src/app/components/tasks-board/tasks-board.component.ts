import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../models/Task';
import {Column} from '../../models/Column';

@Component({
  selector: 'app-tasks-board',
  templateUrl: './tasks-board.component.html',
  styleUrls: ['./tasks-board.component.scss']
})
export class TasksBoardComponent implements OnInit{


  // tasks to show
  @Input('taskList') taskList: Task[];

  // columns to show
  @Input('columnList') columns: Column[];

  // change column task event
  @Output() updateTaskColumnEmmit: EventEmitter<Task> = new EventEmitter<Task>();

  // show task event
  @Output() clickCardEmmit: EventEmitter<Task> = new EventEmitter<Task>();

  // remove column event
  @Output() clickRemoveColumnEmmit: EventEmitter<Column> = new EventEmitter<Column>();

  // drag and drop task
  taskToMove: Task;

  // kebab menu options
  columnOptions: string[];

  constructor() {

    // init data
    this.columnOptions = ['Delete column'];
    this.updateTaskColumnEmmit = new EventEmitter<Task>();
    this.clickCardEmmit = new EventEmitter<Task>();
    this.clickRemoveColumnEmmit = new EventEmitter<Column>();
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void { }

  /**
   * Allow drop elements
   *
   * @param $ev event
   */
  allowDrop($ev): void {
    $ev.preventDefault();
  }

  /**
   * Drag task event
   *
   * @param $event task card
   * @param task Task object to move
   */
  dragCard($event: DragEvent, task: Task): void {

    const element = $event.target as HTMLElement;

    $event.dataTransfer.setData("text/html", element.id);

    this.taskToMove = task;
  }

  /**
   * Drop task event
   *
   * @param $event task card
   * @param column new task Column object
   */
  dropCard($event: DragEvent, column: Column): void {

    $event.preventDefault();

    // get card from dataTransfer and element
    const card = $event.dataTransfer.getData("text/html");
    const element = $event.target as HTMLElement;

    // get element classes
    const classes = element.classList;

    // update model and send event to parent
    this.taskToMove.column = column;
    this.updateTaskColumnEmmit.emit(this.taskToMove);

    // if has column class add the task card directly
    if (classes.contains('column'))  {
      element.appendChild(document.getElementById(card));
      return;
    }

    // else search column element to add it
    element.closest('.column')
      .appendChild(document.getElementById(card));
  }

  /**
   * Card click event
   *
   * @param task Task object clicked
   */
  clickCard(task: Task): void {
    this.clickCardEmmit.emit(task);
  }

  /**
   * 3 dots option menu event
   *
   * @param column Column object clicked
   */
  removeColumn(column: Column): void {
    this.clickRemoveColumnEmmit.emit(column);
  }
}
