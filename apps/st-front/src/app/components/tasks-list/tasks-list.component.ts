import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../models/Task';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit {

  // task list to show
  @Input('taskList') taskList: Task[];

  // card click event
  @Output() clickedCard: EventEmitter<Task>;

  constructor() {
    this.clickedCard = new EventEmitter<Task>();
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void { }

  /**
   * Card click event,
   * call it from the ui
   *
   * @param task Task clicked
   */
  clickCard(task: Task): void {
    this.clickedCard.emit(task);
  }
}
