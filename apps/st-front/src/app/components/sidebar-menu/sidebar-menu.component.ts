import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {MenuOption} from '../../models/MenuOption';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss']
})
export class SidebarMenuComponent implements OnChanges, OnInit {

  // sidebar menu flag
  @Input() isSideMenuShowed: boolean;

  // sidebar options
  @Input() menuOptions: MenuOption[];

  // option selected event
  @Output() optionMenuSelected: EventEmitter<number>;

  // internal sidebar flag
  show: boolean;

  constructor() {
    // init data
    this.show = false;
    this.optionMenuSelected = new EventEmitter<number>();
  }

  /**
   * @inheritDoc
   */
  ngOnChanges(changes: {[propKey: string]: SimpleChange}): void {

    // if it isn't sidebar menu event then exit
    if (!changes.isSideMenuShowed ){
      return;
    }

    // else take event value
    this.show =
      changes.isSideMenuShowed.firstChange ? false : changes.isSideMenuShowed.currentValue;
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {}

  /**
   * Sidebar menu option item click event emit,
   * call it from the ui
   *
   * @param i number option index
   */
  clickOption(i: number): void {
    this.optionMenuSelected.emit(i);
  }
}
