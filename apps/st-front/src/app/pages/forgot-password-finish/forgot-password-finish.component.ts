import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Errors} from '../../helpers/Errors';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-forgot-password-finish',
  templateUrl: './forgot-password-finish.component.html',
  styleUrls: ['./forgot-password-finish.component.scss']
})
export class ForgotPasswordFinishComponent implements OnInit, OnDestroy {

  // Form controls
  password: string ;
  password2: string;
  form: FormGroup;

  // Submitted control
  submitted: boolean ;

  // Encrypted mail
  hash: string;

  // subscriptions array
  subscriptions: Subscription[];

  // Modal to show messages
  @ViewChild("simpleModal", null) simpleModal;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
    this.password = '';
    this.password2 = '';
    this.submitted = false;
    this.hash = '';
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    // get hash from url
    this.hash =
      this.route.snapshot.paramMap.get('hash');

    // validators
    this.form =
      this.formBuilder.group({
      up: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]],
      up2: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]]
    });
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Submit form event,
   * call it from the ui
   */
  onSubmit(): void{
    // change submitted status, this is used in the view to show or not errors
    this.submitted = true;

    // if is an invalid form then exit
    if (this.form.invalid) {
      return;
    }

    // security check
    if (!this.hash || this.hash == '') {
      this.openErrorModal(Errors.HASH_NOT_DEFINED_ERROR);
    }

    // take form values
    this.password = this.f().up.value;
    this.password2 = this.f().up2.value;

    // check if passwords match
    if (this.password != this.password2) {
      this.openErrorModal(Errors.PASSWORDS_MISMATCH_ERROR);
      return;
    }

    // send info to server
    const sub =
      this.userService.forgotChangePass(this.password,this.hash).subscribe(
      () => {
        // if works well then open a dialog to go to login page
        this.openSuccessModal();
      },
      error => {
        // show the error
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  private openErrorModal(errorMsg: string): void{
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a success dialog
   */
  private openSuccessModal(): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = 'Password changed';
    this.simpleModal.modal_button_text = 'Go to login!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Close an open dialog,
   * if it's a success dialog then go to login
   */
  closeModal(): void {
    this.submitted = false;

    if (!this.simpleModal.is_error) {
      this.router.navigate(['/']).then(() => {});
    }
  }

  /**
   * Aux method to sort form controls calls
   */
  private f(): any {
    return this.form.controls;
  }
}
