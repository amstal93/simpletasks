import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Cons} from '../../helpers/Cons';
import {Strings} from '../../helpers/Strings';
import {Subscription} from 'rxjs';

/**
 * Login page component
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  // user
  public user: User;

  // form
  public form: FormGroup;

  // this variable is used in the login view to show form errors
  public submitted: boolean;

  // when is set it then user has a active session
  public token: string;

  // subscriptions array
  subscriptions: Subscription[];

  /** Modal to show messages **/
  @ViewChild("simpleModal", null) simpleModal;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService) {
    this.user = new User();
    this.user.password = '';
    this.user.mail = '';
    this.user.uuid = '';
    this.user.avatar = '';
    this.user.role = null;
    this.user.status = 0;
    this.submitted = false;
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {

    // check if the user is already logged
    const currentUrl = location.pathname;

    // if is already logged and it's login page open success modal
    if (this.userService.isLogged() && currentUrl == '/') {
      this.openSuccessModal(Strings.ALREADY_LOGGED_MESSAGE);
    }

    // else, init form validators
    this.form = this.formBuilder.group({
      um: ['', [Validators.required, Validators.email]],
      up: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Call to the api to try get logged
   */
  login(): void {

    // change submitted status, this is used in the view to show or not errors
    this.submitted = true;

    // if is an invalid form then exit
    if (this.form.invalid) {
      return;
    }

    // else, inform user data and send it by user service login method
    this.user.mail = this.f().um.value;
    this.user.password = this.f().up.value;

    const sub = this.userService.login(this.user).subscribe(
      response => {

        // if user is logged a token is given, save it in local storage
        this.userService.setTokenData(response);

        // show success dialog and go to task list
        this.openSuccessModal(Strings.ALREADY_LOGGED_MESSAGE);
      },
      error => {
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  protected openErrorModal(errorMsg: string): void{
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a success dialog
   */
  protected openSuccessModal(text: string): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = text;
    this.simpleModal.modal_button_text = 'Go ahead!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Close an open dialog,
   * if it's a success dialog then go to task list
   */
  closeModal(): void {
    this.submitted = false;

    if (!this.simpleModal.is_error) {
      location.href = Cons.frontTaskListPath;
    }
  }

  /**
   * Aux method to sort form controls calls
   */
  protected f(): any {
    return this.form.controls;
  }
}
