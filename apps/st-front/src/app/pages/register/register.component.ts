import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {UploaderService} from '../../services/uploader.service';
import {LoginComponent} from '../login/login.component';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Errors} from '../../helpers/Errors';
import {Strings} from '../../helpers/Strings';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends LoginComponent implements OnInit, OnDestroy {

  // repeat password control
  userPass2: string;

  // avatar file selected
  avatarFile: File;

  // subscriptions array
  subscriptions: Subscription[];

  constructor(private registerFormBuilder: FormBuilder,
              private registerUserService: UserService,
              private registerRouter: Router,
              private registerUploadService: UploaderService) {
    super(registerFormBuilder, registerUserService);
    this.userPass2 = '';
    this.user.name = '';
    this.user.password = '';
    this.avatarFile = null;
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    // form validators
    this.form = this.registerFormBuilder.group({
      um: ['', [
        Validators.required,
        Validators.email,
        Validators.maxLength(255)
      ]],
      un: ['', [
        Validators.maxLength(75)
      ]],
      up: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]],
      up_2: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]],
      av: ['']
    });
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Upload avatar file to the server,
   * call it from the ui
   *
   * @param event file form event
   */
  onFileChange(event): void {

    // when an user select an avatar try to upload it
    if (event.target.files.length > 0) {
      const formData = new FormData();
      const file = event.target.files[0] as File;

      const allowedMimeType = [
        'image/jpeg',
        'image/png'
      ];

      // check if is allowed file type
      if (allowedMimeType.indexOf(file.type) == -1) {
        this.avatarFile = null;
        this.user.avatar = '';
        this.openErrorModal(Errors.AVATAR_FORMAT_ERROR);
        return;
      }

      // post file to uploader service
      formData.append('uploadFile', file);

      const sub =
        this.registerUploadService.upload(formData, false).subscribe(
        (res) => {
          // it there is avatar path response save it in user object
          if (res.body) {
            this.user.avatar = res.body.message;
          }
        },
        () => {
          this.user.avatar = null;
          this.avatarFile = null;
          this.openErrorModal(Errors.UPLOAD_FILE_ERROR);
        }
      );

      this.subscriptions.push(sub);
    }
  }

  /**
   * Submit form event,
   * call it from the ui
   */
  onSubmit(): void {
    this.submitted = true;

    // if is an invalid form then exit let the view show the errors
    if (this.form.invalid) {
      return;
    }

    // if password mismatch then exit
    if (!this.checkPasswordsMatch()) {
      return;
    }

    this.user.mail = this.f().um.value;
    this.user.name = this.f().un.value;

    const sub =
      this.registerUserService.register(this.user).subscribe(
      () => {
        // show success dialog and go to login
        this.openSuccessModal(Strings.REGISTERED_SUCCESS_MESSAGE);
      },
      error => {
        // show the error
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Check if password and repeat password match
   *
   * @return boolean
   */
  protected checkPasswordsMatch(): boolean {
    // check passwords
    this.user.password = this.f().up.value.trim();
    this.userPass2 = this.f().up_2.value.trim();

    // if passwords mismatch show an error and exit
    if (this.user.password != this.userPass2) {
      this.openErrorModal(Errors.PASSWORDS_MISMATCH_ERROR);
      return false;
    }

    return true;
  }

  /**
   * Close modal event
   */
  closeModal(): void {

    // if it's a success dialog then go to login
    if (!this.simpleModal.is_error) {
      this.registerRouter.navigate(['/']).then(() => []);
    }
  }
}
