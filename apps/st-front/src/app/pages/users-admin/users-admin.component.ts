import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Role} from '../../models/Role';
import {RoleService} from '../../services/role.service';
import {Router} from '@angular/router';
import {Errors} from '../../helpers/Errors';
import {Strings} from '../../helpers/Strings';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss']
})
export class UsersAdminComponent implements OnInit, OnDestroy {

  // user list
  users: User[];
  // role list
  roles: Role[];
  // user selected to remove
  public userToRemove: User;
  lastErrorCode: number;

  // subscriptions array
  subscriptions: Subscription[];

  /** Modal to show messages **/
  @ViewChild("simpleModal", null) simpleModal;

  constructor(
    private userService: UserService,
    private roleService: RoleService,
    private router: Router
  ) {
    this.users = [];
    this.roles = [];
    this.subscriptions = [];
    this.userToRemove = null;
    this.lastErrorCode = -1;
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    // get users
    const listUserSub = this.userService.list().subscribe(
      (response) => {
        response.result.map(userJson =>{
          // create user lists
          this.users.push(userJson);
        });
      },
      error => {
        // show errors
        const message =
          error.error && error.error.message ?
            error.error.message : Errors.GENERAL_ERROR;

        this.lastErrorCode = error.status;
        // show the error
        this.openErrorModal(message);
      }
    );

    this.subscriptions.push(listUserSub);

    const roleListSub = this.roleService.list().subscribe(
      (response) => {
        // get role list
        response.result.map(roleJson =>{
          this.roles.push(roleJson);
        });

      },
      error => {
        // show errors
        const message =
          error.error && error.error.message ?
            error.error.message : Errors.GENERAL_ERROR;
        // show the error
        this.openErrorModal(message);
      }
    );

    this.subscriptions.push(roleListSub);
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Change user role
   *
   * @param user user selected
   * @param value new role
   */
  changeUserRole(user: User, value: string): void {
    user.role = this.roles.find(role => role.uuid == value);
    const sub = this.userService.edit(user).subscribe(
      () => this.openSuccessModal(Strings.ROLE_CHANGED_SUCCESS_MESSAGE),
      error => this.openErrorModal(error.error.message)
    );

    this.subscriptions.push(sub);
  }

  /**
   * Change user status
   *
   * @param user user to change
   */
  changeUserStatus(user: User): void {
    const sub = this.userService.edit(user).subscribe(
      () => this.openSuccessModal(Strings.STATUS_CHANGED_SUCCESS_MESSAGE),
      error => this.openErrorModal(error.error.message)
    );

    this.subscriptions.push(sub);
  }

  /**
   * Remove an user
   *
   * @param user to remove
   */
  removeUser(user: User) {
    this.userToRemove = user;
    this.openWarningModal(Strings.USER_DELETE_WARNING_MESSAGE + user.name);
  }

  /**
   * Remove user method
   */
  private removeUserFromList(): void{

    const sub = this.userService.remove(this.userToRemove).subscribe(
      () => {

        // remove the user from the ui list
        const removedUsers =
          this.users.splice(this.users.indexOf(this.userToRemove), 1 );

        if (!removedUsers || removedUsers.length == 0) {
          return;
        }

        const removedUser = removedUsers[0];
        const userLogged = this.userService.getTokenDecoded(localStorage.getItem('token'));

        // if is the same user then logout and go to login
        if (userLogged.sub == removedUser.uuid) {
          localStorage.removeItem('token');
          location.href = '/';
          return;
        }


      },
      () => setTimeout(() => this.openErrorModal(Errors.USER_WITH_TASKS_ERROR), 200)
    );
    this.userToRemove = null;

    this.subscriptions.push(sub);
  }

  /**
   * Show a success dialog
   */
  protected openSuccessModal(text): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = text;
    this.simpleModal.modal_button_text = 'Dismiss!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a warning dialog
   */
  protected openWarningModal(text): void{
    this.simpleModal.modal_title = 'Warning';
    this.simpleModal.modal_text = text;
    this.simpleModal.modal_button_text = 'Remove user!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = true;
    this.simpleModal.is_cancellable = true;

    this.simpleModal.openModal();
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  protected openErrorModal(errorMsg: string): void{
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Close modal method
   *
   * @param cancelled if is cancelled
   */
  closeModal(cancelled): void {

    // if is an privileges error then exit
    if (this.lastErrorCode == 403) {
      this.router.navigate(['/']).then(() => []);
      return;
    }

    // if is not a warning dialog (success or error) exit
    if (!this.simpleModal.is_warning) {
      return;
    }

    // if is cancelled exit
    if (cancelled) {
      return;
    }

    // when a warning dialog is shown and isn't cancelled then remove the user selected
    this.removeUserFromList();
  }
}
