import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Cons} from './helpers/Cons';
import {Column} from './models/Column';
import {ColumnService} from './services/column.service';
import {Router} from '@angular/router';
import {MenuOptionService} from './services/menu-option.service';
import {MenuOption} from './models/MenuOption';
import {UserService} from './services/user.service';
import {Errors} from './helpers/Errors';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  // sidebar menu show flag
  isSideMenuShowed: boolean;
  showToolbar: boolean = true;

  // sidebar options
  menuOptions: MenuOption[] = [];
  menuUrl: string[] = [];

  // subscriptions array
  subscriptions: Subscription[];

  // views
  @ViewChild('columnModal', null) columnModal;
  @ViewChild('simpleModal', null) simpleModal;

  constructor(
    private columnService: ColumnService,
    private menuOptionService: MenuOptionService,
    private userService: UserService,
    private router: Router) {
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {

    // check if show or not the toolbar, to do that,
    // get current url and substring to get only the last path
    let currentUrl = location.pathname;
    const firstPathIndex = currentUrl.indexOf('/');
    const lastPathIndex = currentUrl.lastIndexOf('/');

    // only substring it when isn't home
    if (firstPathIndex != lastPathIndex) {
      currentUrl = currentUrl.substr(firstPathIndex, lastPathIndex + 1);
    }

    // urls where hide the toolbar
    const showToolbarUrls = [
      Cons.frontTaskListPath,
      Cons.frontNewTaskPath,
      Cons.frontProfileListPath,
      Cons.frontUpdateTaskPath,
      Cons.frontAdminUser,
    ];

    // if they are matched with someone then hide the toolbar
    // and exit
    if (showToolbarUrls.indexOf(currentUrl) == -1) {
      this.showToolbar = false;
      return;
    }

    // else, we need to prepare the sidebar retrieving options list
    const menuList = this.menuOptionService.list().subscribe(
      res => {
        // the backend doesn't store urls because of that we need
        // a separated array with the urls to go
        this.menuOptions = res.result;
        this.menuUrl = [
          Cons.frontProfileListPath,
          'logOut',
          Cons.frontAdminUser,
          'createColumn',
          Cons.frontNewTaskPath,
          Cons.frontTaskListPath
        ];

        // check if the current user is or not admin
        const currentUser =
          this.userService.getTokenDecoded(localStorage.getItem('token'));

        // if is an admin then exit
        if (currentUser.isAdm == 1){
          return;
        }

        // else, remove admin options from the arrays
        this.menuOptions.splice(2,1);
        this.menuUrl.splice(2,1);

      },
      () => {
        this.openErrorModal(Errors.GET_SIDEBAR_OPTIONS_ERROR);
      }
    );

    this.subscriptions.push(menuList);
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Hamburger menu is pressed,
   * call it from the ui
   * @param isShowed
   */
  showOrHideSideMenu(isShowed: boolean): void {
    this.isSideMenuShowed = isShowed;
  }

  /**
   * Show add column pop up,
   * call it from the ui
   */
  showColumnPopUp(): void {
    this.columnModal.openModal();
  }

  /**
   * When then column dialog closes
   * by clicking save button,
   * call it from the ui
   *
   * @param column Column object retrieved by the modal
   */
   createColumn(column: Column): void {

    // security checks
    if (!column || !column.title) {
      return;
    }

    // no more than five columns are allowed
    const sub = this.columnService.list().subscribe(
      res => {

        // if there are mora than 4 columns show an error and exit
        if (res.result.length > 4) {
          this.openErrorModal(Errors.COLUMN_LIMIT_ERROR);
          return
        }

        // else init empty column fields with dummy data
        column.color = '#FFF';
        column.position = 1;

        // save column
        const createSub = this.columnService.create(column).subscribe(
          () => {

            // if we are in task panel we need to refresh the page
            if (this.router.url == Cons.frontTaskListPath) {
              this.router.routeReuseStrategy.shouldReuseRoute = function () {
                return false;
              };
              this.router.onSameUrlNavigation = 'reload';
              this.router.navigate([Cons.frontTaskListPath]).then(() => {});
              return;
            }

            // else go to task panel
            this.router.navigate([Cons.frontTaskListPath]).then(() => {});
          },
          error => {
            this.openErrorModal(error.error.message);
          }
        );

        this.subscriptions.push(createSub);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Sidebar option click event,
   * call it from the ui
   *
   * @param selectedOption number
   */
   sidebarMenuOptionClick(selectedOption: number): void {

    // get destination url
    const optionSelected = this.menuUrl[selectedOption];

    // if it's logout option then logout
    if (optionSelected === 'logOut') {
      this.logOut();
      return;
    }

    // if it's a create option then show column popup
    if (optionSelected === 'createColumn') {
      this.showColumnPopUp();
      return;
    }

    // else go to selected url
    this.router.navigate([optionSelected]).then(() => {});
  }

  /**
   * Call to UserService.logOut method and
   * go to login
   */
  private logOut(): void {
    this.userService.logOut();
    location.href = '/';
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  private openErrorModal(errorMsg: string): void{
    errorMsg = errorMsg ? errorMsg : 'Error with the server, try again';
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }
}
