import {HttpClient} from '@angular/common/http';
import {ParamMakerService} from './param-maker.service';
import {UserService} from './user.service';
import {Observable, throwError} from 'rxjs';
import {Cons} from '../helpers/Cons';
import {Errors} from '../helpers/Errors';
import {environment} from '../../environments/environment';

export class SimpleTaskService {

  // type of service, is a part of the backend url path
  classType: string;

  constructor(
    private httpClient: HttpClient,
    private paramService: ParamMakerService,
    private usersService: UserService
  ) {
    this.classType = '';
  }

  /**
   * Get an object from the backend given an uuid
   *
   * @param object Only model classes are supported
   */
  show(object: any): Observable<any> {

    // initial check
    if (!this.usersService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json to send
    const url = environment.backBaseApiPath + '/' + this.classType + Cons.showPath + object.uuid;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.httpClient.get(
      url,
      { headers: headers }
    );
  }

  /**
   * Get a list of objects
   */
  list(): Observable<any> {

    // initial check
    if (!this.usersService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json to send
    const url = environment.backBaseApiPath + '/' + this.classType + Cons.listPath;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.httpClient.get(
      url,
      { headers: headers }
    );
  }

  /**
   * Create an object
   *
   * @param object
   */
  create(object: any): Observable<any> {

    // initial check
    if (!this.usersService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/' + this.classType + Cons.createPath;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);
    const params = this.paramService.getParams(object);

    // call the api to try to register the user given
    return this.httpClient.post(
      url,
      params,
      { headers: headers }
    );
  }

  /**
   * Remove an object
   *
   * @param object
   */
  remove(object: any): Observable<any> {

    // initial check
    if (!this.usersService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json
    const url = environment.backBaseApiPath + '/' + this.classType + Cons.removePath + object.uuid;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.httpClient.delete(
      url,
      { headers: headers }
    );
  }

  /**
   * Edit an object
   *
   * @param object
   */
  edit(object: any): Observable<any> {

    // initial check
    if (!this.usersService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/' + this.classType + Cons.editPath + object.uuid;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);
    const params = this.paramService.getParams(object);

    // call the api to try to register the user given
    return this.httpClient.put(
      url,
      params,
      { headers: headers }
    );
  }
}
