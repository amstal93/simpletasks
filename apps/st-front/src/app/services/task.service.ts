import {Injectable} from '@angular/core';
import {SimpleTaskService} from './simple-task.service';
import {HttpClient} from '@angular/common/http';
import {ParamMakerService} from './param-maker.service';
import {UserService} from './user.service';
import {Observable, throwError} from 'rxjs';
import {Cons} from '../helpers/Cons';
import {Column} from '../models/Column';
import {Errors} from '../helpers/Errors';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskService extends SimpleTaskService{

  constructor(private taskClient: HttpClient,
              private taskParamMakerService: ParamMakerService,
              private taskUserService: UserService) {
    super(taskClient, taskParamMakerService, taskUserService);
    this.classType = 'task';
  }

  /**
   * Get tasks inner a column given
   *
   * @param column Column
   * @return Observable<any> get call observable
   */
  listByColumn(column: Column): Observable<any> {

    // initial check
    if (!this.taskUserService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    if (this.classType === '') {
      return throwError(Errors.SERVICE_CLASS_TYPE_NOT_DEFINED_ERROR);
    }

    // create the json to send
    const url =
      environment.backBaseApiPath +
      '/' + this.classType +
      Cons.listByColumnPath + column.uuid;

    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.taskClient.get(
      url,
      { headers: headers }
    );
  }
}
