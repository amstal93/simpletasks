<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="uuid")
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=75)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $color;

		/**
		 * @var Collection $task
		 * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="tags")
		 */
		private $tasks;

	public function __construct() {
		$this->tasks = new ArrayCollection();
	}

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }


	/**
	 * @return Collection
	 */
	public function getTasks(): Collection {
		return $this->tasks;
	}

	public function setTasks(Collection $tasks): self {
		$this->tasks = $tasks;
		return $this;
	}

	public function addTask(Task $task): self {
		$this->getTasks()->add($task);
		return $this;
	}

	public function removeTask(Task $task): self {
		$this->getTasks()->remove($this->getTasks()->indexOf($task));
		return $this;
	}
}
