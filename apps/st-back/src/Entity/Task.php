<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task implements JsonSerializable {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="uuid")
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=75)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $attached;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiration_date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priority;

    /**
     * @var User $assignment
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $assignment;

    /**
     * @var Tag $tags
     * @ORM\ManyToOne(targetEntity="App\Entity\Tag", inversedBy="tags")
     */
    private $tags;

    /**
     * @var Column $_column
     * @ORM\ManyToOne(targetEntity="App\Entity\Column")
     */
    private $_column;

    /**
     * @var Project $project
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     */
    private $project;

	/**
	 * @inheritDoc
	 */
	public function jsonSerialize() {

		if (!$this->assignment) {
			$this->assignment = new User();
			$this->assignment->setRole(new Role());
		}

		if (!$this->tags) {
			$this->tags = new Tag();
		}

		if (!$this->_column) {
			$this->_column = new Column();
		}

		if (!$this->project) {
			$this->project = new Project();
		}

		return [
			'uuid' => $this->uuid,
			'title' => $this->title,
			'summary' => $this->summary,
			'description' => $this->description,
			'attached' => $this->attached,
			'expirationDate' => $this->expiration_date,
			'priority' => $this->priority,
			'assignment' => [
				'uuid' => $this->assignment->getUuid(),
				'name' => $this->assignment->getName(),
				'mail' => $this->assignment->getMail(),
				'role' => [
					'uuid' => $this->assignment->getRole()->getUuid(),
					'name' =>$this->assignment->getRole()->getName(),
					'isAdmin' => $this->assignment->getRole()->getIsAdmin(),
				]
			],
			'tag' => [
				'uuid' => $this->getTags()->getUuid(),
				'title' => $this->getTags()->getTitle(),
				'color' => $this->getTags()->getColor()
			],
			'column' => [
				'uuid' => $this->getColumn()->getUuid(),
				'title' => $this->getColumn()->getTitle(),
				'color' => $this->getColumn()->getColor(),
				'position' => $this->getColumn()->getPosition()
			],
			'project' => [
				'uuid' => $this->getProject()->getUuid(),
				'title' => $this->getProject()->getTitle(),
				'description' => $this->getProject()->getDescription(),
				'color' => $this->getProject()->getColor(),
			]
		];
	}

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAttached(): ?string
    {
        return $this->attached;
    }

    public function setAttached(?string $attached): self
    {
        $this->attached = $attached;

        return $this;
    }

    public function getExpirationDate(): ?DateTimeInterface
    {
        return $this->expiration_date;
    }

    public function setExpirationDate(?DateTimeInterface $expiration_date): self
    {
        $this->expiration_date = $expiration_date;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getAssignment(): ?User
    {
        return $this->assignment;
    }

    public function setAssignment(?User $assignment): self
    {
        $this->assignment = $assignment;

        return $this;
    }

    public function getTags(): ?Tag {
      return $this->tags;
    }

    public function setTags(?Tag $tags): self {
        $this->tags = $tags;
        return $this;
    }

    public function getColumn(): ?Column
    {
        return $this->_column;
    }

    public function setColumn(Column $_column): self
    {
        $this->_column = $_column;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
