<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class FileUploader {
	public function __construct() {
	}

	/**
	 * Move a file from tmp to uploads folder
	 *
	 * @param string $uploadDir
	 * @param string $filename
	 * @param File   $file
	 *
	 * @return string|false real path
	 */
	public function moveFile(string $uploadDir, string $filename, File $file) {

		try {
			$file = $file->move($uploadDir, $filename);
		} catch (FileException $e){
			throw new FileException('Failed to upload file');
		}

		return $file->getRealPath();
	}
}
