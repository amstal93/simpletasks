<?php

namespace App\Service;

use Swift_Mailer;
use Swift_Message;

class MailSender {

	/** @var Swift_Mailer $mailer */
	public $mailer;

	public function __construct( Swift_Mailer $mailer) {
		$this->mailer = $mailer;
	}

	/**
	 * Send an email
	 *
	 * @param string $subject
	 * @param string $to
	 * @param $body
	 *
	 * @return bool return true if success
	 */
	public function send(string $subject, string $to, $body) {
		$message = (new Swift_Message($subject))
			->setFrom('simple.tasks.samir@gmail.com')
			->setTo($to)
			->setBody(
				$body,
				'text/html'
			);

		return !$this->mailer->send($message) == 0;
	}
}
