<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Exception;
use Firebase\JWT\JWT;

class JwtAuth {

	public $entityManager;

	public function __construct(EntityManager $em){
		$this->entityManager = $em;
	}

	/**
	 * Check if user exists in the system, if exists return a token (encoded or
	 * decoded), if not return false
	 *
	 * @param string $mail
	 * @param string $password
	 * @param bool $getDecodedToKen if need a decoded token, user data should be
	 *                              returned
	 *
	 * @return array|bool|object|string
	 */
	public function signUp($mail, $password, $getDecodedToKen = false) {
		// check if user exists
		$user =
			$this->entityManager->getRepository(User::class)->findOneBy([
				'mail' => $mail,
				'password' => $password
			]);

		// if not exists return false
		if (!is_object($user)) {
			return false;
		}

		// if is deactivated return false
		if ($user->getStatus() == 0) {
			return false;
		}

		// generate token
		$now = time();
		$expTime = time() + (3 * 24 * 60 * 60); // three days
		$token = [
			'sub' => $user->getUuid(),
			'name' => $user->getName(),
			'mail' => $user->getMail(),
			'isAdm' => $user->getRole() ? $user->getRole()->getIsAdmin() : 0,
			'iat' => $now,
			'exp' => $expTime
		];
		$token = JWT::encode($token, $_ENV['JWT_KEY'], $_ENV['JWT_ALGO']);

		// if need a decoded token
		if ($getDecodedToKen) {
			$token = JWT::decode($token,$_ENV['JWT_KEY'], [$_ENV['JWT_ALGO']]);
		}

		return $token;
	}

	/**
	 * Check the validity of a token given
	 *
	 * @param string token
	 *
	 * @return bool return true if it's a valid token, false if not
	 */
	public function checkValidToken ($token) {

		// if not exist a token in the request
		if (!$token) {
			return false;
		}

		// try to decode
		try {
			$decoded = JWT::decode($token, $_ENV['JWT_KEY'], [$_ENV['JWT_ALGO']]);
		} catch (Exception $e) {
			// any error sends false
			return false;
		}

		// check if exists sub property to know if it's ok
		if (!$decoded && !is_object($decoded) && !isset($decoded->sub)) {
			return false;
		}

		return true;
	}

	/**
	 * Given a token return an user data if any error occurs then return false
	 *
	 * @param string $token token
	 *
	 * @return bool|object|string return false if any error occurs or an identity
	 *                            if everything is ok
	 */
	public function getIdentity ($token) {
		$identity = false;

		// try to decode
		try {
			$identity = JWT::decode($token, $_ENV['JWT_KEY'], [$_ENV['JWT_ALGO']]);
		} catch (Exception $e) {
			// any error sends false
			return $identity;
		}

		return $identity;
	}

}
