<?php

namespace App\Controller;

use App\Entity\Column;
use App\Entity\Project;
use App\Entity\Tag;
use App\Entity\Task;
use App\Entity\User;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\TaskRepository;
use App\Service\JwtAuth;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class TaskController extends AbstractController {
	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a task, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$summary = $params['summary'] ?? null;
		$description = $params['description'] ?? null;
		$attached = $params['attached'] ?? null;
		$expirationDate = $params['expiration_date'] ?? null;
		$priority = $params['priority'] ?? null;
		
		$assignmentUser = $params['assignment'] ?? null;
		$project = $params['project'] ?? null;
		$tag = $params['tag'] ?? null;
		$column = $params['column'] ?? null;

		// validate params
		if (!$title) {
			return $this->json(Errors::noTaskTitleProvidedError(), 400);
		}

		// set params
		$task = new Task();

		try {
			// try to generate task uuid based on random method
			$task->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		// get object relations
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		// object assignment
		$taskParams = [];

		if ($assignmentUser && isset($assignmentUser['uuid'])) {
			$taskParams['assignmentUuid'] = $assignmentUser['uuid'];
		}
		if ($column && isset($column['uuid'])) {
			$taskParams['columnUuid'] = $column['uuid'];
		}
		if ($project && isset($project['uuid'])) {
			$taskParams['projectUuid'] = $project['uuid'];
		}
		if ($tag && isset($tag['uuid'])) {
			$taskParams['tagUuid'] = $tag['uuid'];
		}
		else if ($tag && !empty($tag['title'])) {
			$newTag = new Tag();
			// try to generate task uuid based on random method
			try {
				$newTag->setUuid(Uuid::uuid4());
			} catch (Exception $e) {
				// an exception occurs inner uuid library
				return $this->json(Errors::uuidError(), 500);
			}
			$newTag->setTitle($tag['title']);
			$newTag->setColor($tag['color']);

			// try to persist
			try {
				$em->persist($newTag);
				$em->flush();
			} catch (ORMException $e) {
				return $this->json(Errors::generalDBError(), 500);
			}
			$taskParams['tagUuid'] = $newTag->getUuid();
		}

		$taskResult = $this->setTaskObjectParams($em, $taskParams, $task);
		$task = $taskResult['task'];

		// common parameters
		$task->setTitle($title);
		$task->setSummary($summary);
		$task->setDescription($description);
		$task->setAttached($attached);
		$task->setPriority($priority);
		try {
			$task->setExpirationDate(new DateTime($expirationDate));
		} catch (Exception $e) {
			return $this->json(Errors::dateFormatError(), 400);
		}

		// try to persist
		try {
			$em->persist($task);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Task created'
		];

		return $this->json($data);
	}

	/**
	 * Shows a task, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get task from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TaskRepository $taskRepo */
		$taskRepo = $em->getRepository(Task::class);

		/** @var Task $taskToShow */
		$taskToShow = $taskRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this task isn't found
		if (!is_object($taskToShow)) {
			return $this->json(Errors::taskNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $taskToShow
		];

		return $this->json($data);
	}

	/**
	 * Show all tasks, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse|Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$taskRepo = $this->getDoctrine()->getRepository(Task::class);
		$task = $taskRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($task,$serializer);
		return $response;
	}

	/**
	 * Show all tasks given their column, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @param string $columnUuid
	 *
	 * @return JsonResponse|Response response
	 */
	public function listByColumn(Request $request, JwtAuth $jwtAuth, $columnUuid) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get column from repo
		$columnRepo =
			$this->getDoctrine()->getRepository(Column::class);
		$column = $columnRepo->findOneBy(['uuid' => $columnUuid]);

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$taskRepo = $this->getDoctrine()->getRepository(Task::class);
		$tasks = $taskRepo->findBy(['_column' => $column->getId()]);

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($tasks,$serializer);
		return $response;
	}

	/**
	 * Edit a task, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToChange uuid to change
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, JwtAuth $jwtAuth, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		$uuidToChange = trim($uuidToChange);

		// check uuid
		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get task from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TaskRepository $taskRepo */
		$taskRepo = $em->getRepository(Task::class);

		/** @var Task $taskToChange */
		$taskToChange = $taskRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this task isn't found
		if (!is_object($taskToChange)) {
			return $this->json(Errors::taskNotFoundError(), 404);
		}

		// get params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$summary = $params['summary'] ?? null;
		$description = $params['description'] ?? null;
		$attached = $params['attached'] ?? null;
		$expirationDate = $params['expiration_date'] ?? null;
		$priority = $params['priority'] ?? null;

		$assignmentUuid = $params['assignment'] ? $params['assignment']['uuid'] : null;
		$projectUuid = $params['project'] ? $params['project']['uuid'] : null;
		$tagUuid = $params['tag'] ? $params['tag']['uuid'] : null;
		$columnUuid = $params['column'] ? $params['column']['uuid'] : null;

		// set params
		$someChange = false;

		if ($title && trim($title) != $taskToChange->getTitle()) {
			$someChange = true;
			$taskToChange->setTitle($title);
		}

		if ($summary && $summary != $taskToChange->getSummary()) {
			$someChange = true;
			$taskToChange->setSummary($summary);
		}

		if ($description && $description != $taskToChange->getDescription()) {
			$someChange = true;
			$taskToChange->setDescription($description);
		}

		if ($attached && trim($attached) != $taskToChange->getAttached()) {
			$someChange = true;
			$taskToChange->setAttached($attached);
		}

		if ($expirationDate && trim($expirationDate) != $taskToChange->getExpirationDate()) {
			$someChange = true;
			try {
				$taskToChange->setExpirationDate(new DateTime($expirationDate));
			} catch (Exception $e) {
				return $this->json(Errors::dateFormatError(), 400);
			}
		}

		if ($priority && trim($priority) != $taskToChange->getPriority()) {
			$someChange = true;
			$taskToChange->setPriority($priority);
		}

		// set object params
		$taskParams['assignmentUuid'] = $assignmentUuid;
		$taskParams['columnUuid'] = $columnUuid;
		$taskParams['projectUuid'] = $projectUuid;
		$taskParams['tagUuid'] = $tagUuid;
		$taskResult = $this->setTaskObjectParams($em, $taskParams, $taskToChange);

		if ($taskResult['someChanges']) {
			$someChange = true;
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the task
		try {

			$em->persist($taskToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'Task edited'
		];

		return $this->json($data);
	}

	/**
	 * Remove a task, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete uuid to delete
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, JwtAuth $jwtAuth, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get task from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TaskRepository $taskRepo */
		$taskRepo = $em->getRepository(Task::class);

		/** @var Task $taskToDelete */
		$taskToDelete = $taskRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this task isn't found
		if (!is_object($taskToDelete)) {
			return $this->json(Errors::taskNotFoundError(), 404);
		}

		// try to remove
		try {
			$em->remove($taskToDelete);
			$em->flush();
		} catch (Exception $e) {
			return $this->json(Errors::generalDBError());
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Task removed'
		];

		return $this->json($data);
	}

	/**
	 * Set task objects like user, tag, etc if exists
	 *
	 * @param EntityManager $em entity manager
	 * @param array $params params to set
	 * @param Task $taskToSet task
	 *
	 * @return array same task from in params and if any change is perform
	 */
	private function setTaskObjectParams(EntityManager $em, array $params, Task $taskToSet) {

		$someChanges = false;

		// get all params
		$assignmentUuid = $params['assignmentUuid'] ?? null;
		$columnUuid = $params['columnUuid'] ?? null;
		$projectUuid = $params['projectUuid'] ?? null;
		$tagUuid = $params['tagUuid'] ?? null;

		// user assigment
		if ($assignmentUuid) {
			$userRepo = $em->getRepository(User::class);
			$user = $userRepo->findOneBy(['uuid' => $assignmentUuid]);

			if (is_object($user)) {
				$taskToSet->setAssignment($user);
				$someChanges = true;
			}
		}

		// column
		if ($columnUuid) {
			$colRepo = $em->getRepository(Column::class);
			$column = $colRepo->findOneBy(['uuid' => $columnUuid]);

			if (is_object($column)) {
				$taskToSet->setColumn($column);
				$someChanges = true;
			}
		}

		// project
		if ($projectUuid) {
			$projectRepo = $em->getRepository(Project::class);
			$project = $projectRepo->findOneBy(['uuid' => $projectUuid]);

			if (is_object($project)) {
				$taskToSet->setProject($project);
				$someChanges = true;
			}
		}

		// tag
		if ($tagUuid) {
			$tagRepo = $em->getRepository(Tag::class);
			$tag = $tagRepo->findOneBy(['uuid' => $tagUuid]);

			if (is_object($tag)) {
				$taskToSet->setTags($tag);
				$someChanges = true;
			}
		}

		return ['task' => $taskToSet, 'someChanges' => $someChanges];
	}
}
