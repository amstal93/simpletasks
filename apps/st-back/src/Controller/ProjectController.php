<?php

namespace App\Controller;

use App\Entity\Project;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\ProjectRepository;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class ProjectController extends AbstractController {
	
	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a project, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$description = $params['description'] ?? null;
		$color = $params['color'] ?? null;

		// validate params
		if (!$title) {
			return $this->json(Errors::noProjectTitleProvidedError(), 400);
		}

		// set params
		$project = new Project();

		try {
			// try to generate project uuid based on random method
			$project->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		$project->setTitle($title);
		$project->setDescription($description);
		$project->setColor($color);

		// try to persist
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		try {
			$em->persist($project);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Project created'
		];

		return $this->json($data);
	}

	/**
	 * Shows a project, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get project from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ProjectRepository $projectRepo */
		$projectRepo = $em->getRepository(Project::class);

		/** @var Project $projectToShow */
		$projectToShow = $projectRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this project isn't found
		if (!is_object($projectToShow)) {
			return $this->json(Errors::projectNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $projectToShow
		];

		return $this->json($data);
	}

	/**
	 * Show all projects, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse|Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$projectRepo = $this->getDoctrine()->getRepository(Project::class);
		$project = $projectRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($project,$serializer);
		return $response;
	}

	/**
	 * Edit a project, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToChange uuid to change
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, JwtAuth $jwtAuth, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// check uuid
		$uuidToChange = trim($uuidToChange);

		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get project from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ProjectRepository $projectRepo */
		$projectRepo = $em->getRepository(Project::class);

		/** @var Project $projectToChange */
		$projectToChange = $projectRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this project isn't found
		if (!is_object($projectToChange)) {
			return $this->json(Errors::projectNotFoundError(), 404);
		}

		// get params
		$params = json_decode($json, true);
		$title = $params["title"] ?? null;
		$color = $params["color"] ?? null;
		$description = $params['description'] ?? null;

		// set params
		$someChange = false;

		if ($title && trim($title) != $projectToChange->getTitle()) {
			$someChange = true;
			$projectToChange->setTitle($title);
		}

		if ($description && $description != $projectToChange->getDescription()) {
			$someChange = true;
			$projectToChange->setDescription($description);
		}

		if ($color && trim($color) != $projectToChange->getColor()) {
			$someChange = true;
			$projectToChange->setColor($color);
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the project
		try {

			$em->persist($projectToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'Project edited'
		];

		return $this->json($data);
	}

	/**
	 * Remove a project, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete uuid to delete
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, JwtAuth $jwtAuth, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get project from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ProjectRepository $projectRepo */
		$projectRepo = $em->getRepository(Project::class);

		/** @var Project $projectToDelete */
		$projectToDelete = $projectRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this project isn't found
		if (!is_object($projectToDelete)) {
			return $this->json(Errors::projectNotFoundError(), 404);
		}

		// try to remove
		try {
			$em->remove($projectToDelete);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Project removed'
		];

		return $this->json($data);
	}
}
