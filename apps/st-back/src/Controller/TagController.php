<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\TagRepository;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class TagController extends AbstractController {
	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a tag, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$color = $params['color'] ?? null;

		// validate params
		if (!$title) {
			return $this->json(Errors::noTagTitleProvidedError(), 400);
		}

		// set params
		$tag = new Tag();

		try {
			// try to generate tag uuid based on random method
			$tag->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		$tag->setTitle($title);
		$tag->setColor($color);

		// try to persist
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		try {
			$em->persist($tag);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Tag created',
			'tag' => $tag
		];

		return $this->json($data);
	}

	/**
	 * Shows a tag, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get tag from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TagRepository $tagRepo */
		$tagRepo = $em->getRepository(Tag::class);

		/** @var Tag $tagToShow */
		$tagToShow = $tagRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this tag isn't found
		if (!is_object($tagToShow)) {
			return $this->json(Errors::tagNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $tagToShow
		];

		return $this->json($data);
	}

	/**
	 * Show all tags, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse|Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');

		$tagRepo = $this->getDoctrine()->getRepository(Tag::class);
		$tag = $tagRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($tag,$serializer);
		return $response;
	}

	/**
	 * Edit a tag, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToChange uuid to change
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, JwtAuth $jwtAuth, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// check uuid
		$uuidToChange = trim($uuidToChange);
		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get tag from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TagRepository $tagRepo */
		$tagRepo = $em->getRepository(Tag::class);

		/** @var Tag $tagToChange */
		$tagToChange = $tagRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this tag isn't found
		if (!is_object($tagToChange)) {
			return $this->json(Errors::tagNotFoundError(), 404);
		}

		// get params
		$params = json_decode($json, true);
		$title = $params["title"] ?? null;
		$color = $params["color"] ?? null;

		// set params
		$someChange = false;

		if ($title && trim($title) != $tagToChange->getTitle()) {
			$someChange = true;
			$tagToChange->setTitle($title);
		}

		if ($color && trim($color) != $tagToChange->getColor()) {
			$someChange = true;
			$tagToChange->setColor($color);
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the tag
		try {

			$em->persist($tagToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'Tag edited'
		];

		return $this->json($data);
	}

	/**
	 * Remove a tag, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete uuid to delete
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, JwtAuth $jwtAuth, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get tag from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TagRepository $tagRepo */
		$tagRepo = $em->getRepository(Tag::class);

		/** @var Tag $tagToDelete */
		$tagToDelete = $tagRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this tag isn't found
		if (!is_object($tagToDelete)) {
			return $this->json(Errors::tagNotFoundError(), 404);
		}

		// try to remove
		try {
			$em->remove($tagToDelete);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Tag removed'
		];

		return $this->json($data);
	}

	/**
	 * Search a tag given a title
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth auth service
	 * @param string $title tag title to search
	 *
	 * @return Response json with all coincidences
	 */
	public function searchByTitle(Request $request, JwtAuth $jwtAuth, string $title) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// create the query and execute it

		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var TagRepository $tagRepo */
		$tagRepo = $em->getRepository(Tag::class);

		$query = $tagRepo->createQueryBuilder('t');
		$query->addSelect(
			"
		 (CASE WHEN t.title like '". $title ."' THEN 0
           WHEN t.title like '". $title ."%' THEN 1
           WHEN t.title like '%". $title ."' THEN 2
           WHEN t.title like '%". $title ."%' THEN 3
           ELSE 4 
      END) 
      AS HIDDEN ORD ");
		$query->orderBy('ORD', 'ASC');

		$tags = $query->getQuery()->getResult();

		// serialize tags to send it in the response

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($tags,$serializer);

		return $response;
	}
}
