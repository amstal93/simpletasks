<?php

namespace App\Controller;

use App\Entity\MenuOption;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\MenuOptionRepository;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class MenuOptionController extends AbstractController {
	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a menuOption, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$icon = $params['icon'] ?? null;

		// validate params
		if (!$title) {
			return $this->json(Errors::noMenuOptionTitleProvidedError(), 400);
		}

		if (!$icon) {
			return $this->json(Errors::noMenuIconTitleProvidedError(), 400);
		}

		// set params
		$menuOption = new MenuOption();

		try {
			// try to generate menuOption uuid based on random method
			$menuOption->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		$menuOption->setTitle($title);
		$menuOption->setIcon($icon);

		// try to persist
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		try {
			$em->persist($menuOption);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'MenuOption created'
		];

		return $this->json($data);
	}

	/**
	 * Shows a menuOption, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get menuOption from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var MenuOptionRepository $menuOptionRepo */
		$menuOptionRepo = $em->getRepository(MenuOption::class);

		/** @var MenuOption $menuOptionToShow */
		$menuOptionToShow = $menuOptionRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this menuOption isn't found
		if (!is_object($menuOptionToShow)) {
			return $this->json(Errors::menuOptionNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $menuOptionToShow
		];

		return $this->json($data);
	}

	/**
	 * Show all menuOptions, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse|Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$menuOptionRepo = $this->getDoctrine()->getRepository(MenuOption::class);
		$menuOption = $menuOptionRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($menuOption,$serializer);
		return $response;
	}

	/**
	 * Edit a menuOption, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToChange uuid to change
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, JwtAuth $jwtAuth, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// check uuid
		$uuidToChange =  trim($uuidToChange);
		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get menuOption from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var MenuOptionRepository $menuOptionRepo */
		$menuOptionRepo = $em->getRepository(MenuOption::class);

		/** @var MenuOption $menuOptionToChange */
		$menuOptionToChange = $menuOptionRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this menuOption isn't found
		if (!is_object($menuOptionToChange)) {
			return $this->json(Errors::menuOptionNotFoundError(), 404);
		}

		// get params
		$params = json_decode($json, true);
		$title = $params["title"] ?? null;
		$icon = $params["icon"] ?? null;

		// set params
		$someChange = false;

		if ($title && trim($title) != $menuOptionToChange->getTitle()) {
			$someChange = true;
			$menuOptionToChange->setTitle($title);
		}

		if ($icon && trim($icon) != $menuOptionToChange->getIcon()) {
			$someChange = true;
			$menuOptionToChange->setIcon($icon);
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the menuOption
		try {

			$em->persist($menuOptionToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'MenuOption edited'
		];

		return $this->json($data);
	}

	/**
	 * Remove a menuOption, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete uuid to delete
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, JwtAuth $jwtAuth, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get menuOption from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var MenuOptionRepository $menuOptionRepo */
		$menuOptionRepo = $em->getRepository(MenuOption::class);

		/** @var MenuOption $menuOptionToDelete */
		$menuOptionToDelete = $menuOptionRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this menuOption isn't found
		if (!is_object($menuOptionToDelete)) {
			return $this->json(Errors::menuOptionNotFoundError(), 404);
		}

		// try to remove
		try {
			$em->remove($menuOptionToDelete);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'MenuOption removed'
		];

		return $this->json($data);
	}
}
