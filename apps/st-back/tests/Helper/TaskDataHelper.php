<?php

namespace App\Tests\Helper;

class TaskDataHelper {

	public static function getTaskUuid() {
		return '4e482a5f-edd9-46f4-ba77-9c978f292768';
	}

	public static function getTaskUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}
}
