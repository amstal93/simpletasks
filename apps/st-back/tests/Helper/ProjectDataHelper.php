<?php

namespace App\Tests\Helper;

class ProjectDataHelper {

	public static function getProjectUuid() {
		return '5f7f649c-e543-4cec-9ea6-f241bd151776';
	}

	public static function getProjectUuidWithoutTasks() {
		return '41e4ffc2-d40a-4482-9246-36605ef32535';
	}

	public static function getProjectUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}
}
