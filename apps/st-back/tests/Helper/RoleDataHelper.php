<?php

namespace App\Tests\Helper;

class RoleDataHelper {

	public static function getRoleUuid() {
		return 'b2f6a252-9e9b-4624-ae9d-cfb76be6fdd0';
	}

	public static function getOtherRoleUuid() {
		return '0b6e739e-8374-454e-ba1e-c826c277e88d';
	}

	public static function getRoleUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}
}
