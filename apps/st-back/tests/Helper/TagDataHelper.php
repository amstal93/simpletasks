<?php

namespace App\Tests\Helper;

class TagDataHelper {

	public static function getTagUuid() {
		return 'c33901fa-5723-4fa3-91b2-618dba18378a';
	}

	public static function getTagUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}

	public static function getTagTitleFound() {
		return 'ddddd';
	}
}
