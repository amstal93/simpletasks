<?php

namespace App\Tests\Helper;

class MenuOptionDataHelper {

	public static function getMenuOptionUuid() {
		return '89bf61b3-e21f-4136-87ac-b22b0f013c4e';
	}

	public static function getMenuOptionUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}
}
