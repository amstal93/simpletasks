<?php

namespace App\Tests\Helper;

class ColumnDataHelper {

	public static function getColumnUuid() {
		return '0166c7de-5d34-4987-82bb-77c218d281ef';
	}

	public static function getColumnUuidNotFound() {
		return '1c2175c9-b9c8-4d41-80fc-ce11f852ded4';
	}
}
