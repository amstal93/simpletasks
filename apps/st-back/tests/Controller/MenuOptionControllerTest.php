<?php
namespace App\Tests\Controller;

use App\Tests\Helper\MenuOptionDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MenuOptionControllerTest extends WebTestCase {

	public function testCreateMenuOptionOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/menuoption/create',[
			'json' => json_encode([
				'title' => 'MenuOptionTest1',
				'icon' => 'icon.jpg'
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateMenuOptionInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/menuoption/create',[
			'json' => json_encode([
				'title' => 'MenuOptionTest1',
				'icon' => 'icon.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateMenuOptionInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/menuoption/create');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateMenuOptionNoTitleProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/menuoption/create',[
			'json' => json_encode([
				'title' => '',
				'icon' => 'icon.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateMenuOptionNoIconProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/menuoption/create',[
			'json' => json_encode([
				'title' => 'title1',
				'icon' => ''
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowMenuOptionOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/menuoption/show/' . MenuOptionDataHelper::getMenuOptionUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowMenuOptionInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/menuoption/show/' . MenuOptionDataHelper::getMenuOptionUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowMenuOptionNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/menuoption/show/' . MenuOptionDataHelper::getMenuOptionUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListMenuOptionOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/menuoption/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListMenuOptionInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/menuoption/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditMenuOptionOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/menuoption/edit/' . MenuOptionDataHelper::getMenuOptionUuid(),
			[
				'json' => json_encode([
					'title' => 'MenuOptionTest1',
					'icon' => 'icon.jpg'
				])
			]
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditMenuOptionInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/menuoption/edit/' . MenuOptionDataHelper::getMenuOptionUuid(),
			[
			'json' => json_encode([
				'title' => 'MenuOptionTest1',
				'icon' => 'icon.jpg'
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditMenuOptionInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/menuoption/edit/' . MenuOptionDataHelper::getMenuOptionUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditMenuOptionNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT', '/api/v1/menuoption/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditMenuOptionNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/menuoption/edit/' . MenuOptionDataHelper::getMenuOptionUuidNotFound(),
			[
				'json' => json_encode([
					'title' => 'MenuOptionTest1',
					'icon' => 'icon.jpg'
				])
			]
		);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testRemoveMenuOptionOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/menuoption/remove/' . MenuOptionDataHelper::getMenuOptionUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveMenuOptionInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/menuoption/remove/' . MenuOptionDataHelper::getMenuOptionUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveMenuOptionNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/menuoption/remove/' . MenuOptionDataHelper::getMenuOptionUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}
}
