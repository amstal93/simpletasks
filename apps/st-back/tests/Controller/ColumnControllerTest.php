<?php
namespace App\Tests\Controller;

use App\Tests\Helper\ColumnDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ColumnControllerTest extends WebTestCase {

	public function testCreateColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/column/create',[
			'json' => json_encode([
				'title' => 'testColumn5',
				'color' => '#FFF',
				'position' => 9
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/column/create',[
			'json' => json_encode([
				'title' => 'testColumn1',
				'color' => '#FFF',
				'position' => 0
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateColumnInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/column/create');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateColumnNoTitleProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/column/create',[
			'json' => json_encode([
				'title' => '',
				'color' => '#FFF',
				'position' => 0
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateColumnNoPositionProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/column/create',[
			'json' => json_encode([
				'title' => 'Title',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/column/show/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/column/show/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowColumnNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/column/show/' . ColumnDataHelper::getColumnUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/column/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/column/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/column/edit/' . ColumnDataHelper::getColumnUuid(),
			[
				'json' => json_encode([
					'title' => 'testColumn9',
					'color' => '#FF0',
					'position' => 9
				])
			]
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/column/edit/' . ColumnDataHelper::getColumnUuid(),
			[
			'json' => json_encode([
				'title' => 'testColumn1',
				'color' => '#FFF',
				'position' => 0
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/column/edit/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT', '/api/v1/column/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/column/edit/' . ColumnDataHelper::getColumnUuidNotFound(),
			[
				'json' => json_encode([
					'title' => 'testColumn1',
					'color' => '#FFF',
					'position' => 0
				])
			]
		);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditColumnNothingProvidedOrAreTheSame() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/column/edit/' . ColumnDataHelper::getColumnUuid(),
			[
				'json' => json_encode([
					'title' => '',
					'color' => '',
					'position' => ''
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/column/remove/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/column/remove/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveColumnNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/column/remove/' . ColumnDataHelper::getColumnUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}
}
