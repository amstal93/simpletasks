<?php
namespace App\Tests\Controller;

use App\Tests\Helper\RoleDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase {
	
	public function testLoginOk() {
		$client = static::createClient();
		$client->request('POST', '/api/v1/login',[
				'json' => json_encode([
					'mail' => 'samir@samsoft.es',
					'password' => '1234'
				])
			]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testLoginJsonEmpty() {
		$client = static::createClient();
		$client->request('POST', '/api/v1/login',[]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testLoginInvalidEmail() {
		$client = static::createClient();
		$client->request('POST', '/api/v1/login',[
			'json' => json_encode([
				'mail' => 'samirrsamsofttt.es',
				'password' => '1234'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testLoginPassEmpty() {
		$client = static::createClient();
		$client->request('POST', '/api/v1/login',[
			'json' => json_encode([
				'mail' => 'samirr@samsofttt.es',
				'password' => ''
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testLoginIncorrectUserOrPass() {
		$client = static::createClient();
		$client->request('POST', '/api/v1/login',[
			'json' => json_encode([
				'mail' => 'samirraaaaa@samsofttt.es',
				'password' => '1234'
			])
		]);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testShowOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/user/show/' . UserDataHelper::getLoggedUuid()
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowWithoutPrivileges() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getLoggedToken());
		$client->request('GET',
			'/api/v1/user/show/' . UserDataHelper::getAdminUuid()
		);

		$this->assertEquals(403, $client->getResponse()->getStatusCode());
	}

	public function testShowInvalidToken() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/user/show/' . UserDataHelper::getLoggedUuid()
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowWithoutUuid() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/user/show/ '
		);

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
			'json' => json_encode([
				'uuid' => UserDataHelper::getLoggedUuid(),
				'name' => 'sam',
				'mail' => 'sam@sam.es',
				'password' => '12345',
				'avatar' => 'avatars.jpg',
				'role' => RoleDataHelper::getRoleUuid(),
				'status' => '1'
			])]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditInvalidToken() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'uuid' => UserDataHelper::getLoggedUuid(),
					'name' => 'sam',
					'mail' => 'sam@sam.es',
					'password' => '1234',
					'avatar' => 'avatar.jpg',
					'role' => RoleDataHelper::getRoleUuid(),
					'status' => '1'
				])]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditWithoutJson() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditWithoutUuid() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'name' => 'sam',
					'mail' => 'sam@sam.es',
					'password' => '1234',
					'avatar' => 'avatar.jpg',
					'role' => RoleDataHelper::getRoleUuid(),
					'status' => '1'
				])]);

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditWithoutParams() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'uuid' => UserDataHelper::getLoggedUuid(),
					'name' => '',
					'mail' => '',
					'password' => '',
					'avatar' => '',
					'role' => '',
					'status' => ''
				])]);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditWithoutPrivileges() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getLoggedToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'uuid' => UserDataHelper::getAdminUuid(),
					'name' => 'sam',
					'mail' => 'sam@sam.es',
					'password' => '1234',
					'avatar' => 'avatar.jpg',
					'role' => RoleDataHelper::getRoleUuid(),
					'status' => '1'
				])]);

		$this->assertEquals(403, $client->getResponse()->getStatusCode());
	}

	public function testEditEmailWrong() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'uuid' => UserDataHelper::getLoggedUuid(),
					'name' => 'sam',
					'mail' => 'samsam.es',
					'password' => '1234',
					'avatar' => 'avatar.jpg',
					'role' => RoleDataHelper::getRoleUuid(),
					'status' => '1'
				])]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditEmailTaken() {

		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/user/edit',[
				'json' => json_encode([
					'uuid' => UserDataHelper::getLoggedUuid(),
					'name' => 'sam',
					'mail' => 'samir@samsoft.es',
					'password' => '1234',
					'avatar' => 'avatar.jpg',
					'role' => RoleDataHelper::getOtherRoleUuid(),
					'status' => '0'
				])]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListWithAValidToken() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET', '/api/v1/user/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListWithAnInvalidToken() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET', '/api/v1/user/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testListWithoutEnoughPermissions() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getLoggedToken());
		$client->request('GET', '/api/v1/user/list');

		$this->assertEquals(403, $client->getResponse()->getStatusCode());
	}

	public function testRemoveOk() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/user/remove/' . UserDataHelper::getLoggedUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveInvalidToken() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/user/remove/'.UserDataHelper::getLoggedUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveNoUuidProvided() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/user/remove/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testRemoveInsufficientPrivileges() {
		
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getLoggedToken());
		$client->request('DELETE',
			'/api/v1/user/remove/'.UserDataHelper::getAdminUuid());

		$this->assertEquals(403, $client->getResponse()->getStatusCode());
	}

	public function testRegisterOk() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[
			'json' => json_encode([
				'name' => 'samirrrrr',
				'mail' => 'samirrr@samsoftttt.es',
				'password' => '1234',
				'avatar' => 'avatar.jpg'
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());

	}

	public function testRegisterJsonEmpty() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRegisterNameEmpty() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[
			'json' => json_encode([
				'name' => '',
				'mail' => 'samirr@samsoftttt.es',
				'password' => '1234',
				'avatar' => 'avatar.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRegisterMailEmpty() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[
			'json' => json_encode([
				'name' => 'samirrrr',
				'mail' => '',
				'password' => '1234',
				'avatar' => 'avatar.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRegisterPassEmpty() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[
			'json' => json_encode([
				'name' => 'samirrrr',
				'mail' => 'samirr@samsoftttt.es',
				'password' => '',
				'avatar' => 'avatar.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRegisterDuplicatedUser() {
		
		$client = static::createClient();
		$client->request('POST', '/api/v1/register',[
			'json' => json_encode([
				'name' => 'samir',
				'mail' => 'samir@samsoft.es',
				'password' => '1234',
				'avatar' => 'avatar.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testSearchByNameOk() {

		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET', '/api/v1/user/findbyname/' . UserDataHelper::getUserNameFound());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testSearchByNameInvalidToken() {

		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET', '/api/v1/user/findbyname/' . UserDataHelper::getUserNameFound());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}
}
